package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getPersona()
                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setConsulta(dto.getConsulta());
        entity.setProfesion(dto.getProfesion());
        entity.setPersona(dto.getPersona());
        return entity;
    }

    public MedicoEnteroDto entityToEnteroDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoEnteroDto(
                                ent.getId(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersona(),
                                ent.getPersona()
                        )
                )
                .orElse(new MedicoEnteroDto());
    }
    public Medico enterodtoToEntity(MedicoEnteroDto dto){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(dto.getIdpersona());
        entity.setPersona(dto.getPersona());
        return entity;
    }
}
