package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {

    public TurnoDto entityToDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoDto());
    }
    public Turno dtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }
    public TurnoEnteroDto entityToEnteroDto(Turno entity){

        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getPaciente(),
                                ent.getMedico(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoEnteroDto());
    }
    public Turno enterodtoToEntity(TurnoEnteroDto dto){
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setPaciente(dto.getPaciente());
        entity.setMedico(dto.getMedico());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }

}
