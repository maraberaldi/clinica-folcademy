package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component

public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getIdpersona(),
                                ent.getPersona()
                        )
                )
                .orElse(new PacienteDto());
    }
    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setIdpersona(dto.getIdpersona());
        entity.setPersona(dto.getPersona());
        return entity;
    }
    public PacienteEnteroDto entityToEnteroDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteEnteroDto(
                                ent.getId(),
                                ent.getIdpersona(),
                                ent.getPersona()
                        )
                )
                .orElse(new PacienteEnteroDto());
    }
    public Paciente enterodtoToEntity(PacienteEnteroDto dto){
        Paciente entity = new Paciente();
        entity.setId(dto.getIdpaciente());
       entity.setIdpersona(dto.getIdpersona());
       entity.setPersona(dto.getPersona());
        return entity;
    }
}
