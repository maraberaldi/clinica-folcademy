package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoEnteroDto {
    Integer id;
    String profesion = "";
    int consulta = 0;
    Integer idpersona;
    Persona persona;
}
