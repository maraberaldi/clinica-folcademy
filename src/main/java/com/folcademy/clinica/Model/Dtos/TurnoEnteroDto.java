package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoEnteroDto {

    Integer idturno;
    LocalDate fecha;
    LocalTime hora;
    Paciente paciente;
    Medico medico;
    Integer idpaciente;
    Integer idmedico;

}
