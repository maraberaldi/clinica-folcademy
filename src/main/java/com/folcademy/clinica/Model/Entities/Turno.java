package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Table(name = "turno")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;
    @Column(name = "hora", columnDefinition = "TIME")
    public LocalTime hora;
    @Column(name = "atendido", columnDefinition = "TINYINT")
    public Boolean atendido;
    @Column(name = "idpaciente", columnDefinition = "INT")
    public Integer idpaciente;
    @Column(name = "idmedico", columnDefinition = "INT")
    public Integer idmedico;

    @ManyToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente",insertable = false,updatable = false)
    private Paciente paciente;

    @ManyToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico",insertable = false,updatable = false)
    private Medico medico;


    public void fecha(LocalDate fecha) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return id != null && Objects.equals(id, turno.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public Integer getIdturno() {
        return id;
    }

    public void setIdturno(Integer idturno) {
        this.id = idturno;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public Boolean getAtendido() {
        return atendido;
    }

    public void setAtendido(Boolean atendido) {
        this.atendido = atendido;
    }

    public Integer getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(Integer idpaciente) {
        this.idpaciente = idpaciente;
    }

    public Integer getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(Integer idmedico) {
        this.idmedico = idmedico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }
}
