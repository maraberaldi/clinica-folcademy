package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TurnoRepository extends PagingAndSortingRepository<Turno, Integer> {
    Page<Turno> findById(Integer id, Pageable pageable);
    Page<Turno> findAll(Pageable pageable);
}
