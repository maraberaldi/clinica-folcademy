package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends PagingAndSortingRepository<Persona, Integer> {
    Page<Persona> findById(Integer id, Pageable pageable);
    Page<Persona> findAll(Pageable pageable);
}
