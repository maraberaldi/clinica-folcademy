package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PacienteDto;

import java.util.List;

public interface IPacienteService {

    List<PacienteDto> findAllPacientes();
    List<PacienteDto> findPacientebyId(Integer id);
}
