package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PersonaDto;

import java.util.List;

public interface IPersonaService {
    List<PersonaDto> findAllPersonas();
    List<PersonaDto> findPersonabyId(Integer id);
}
