package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;

import java.util.List;

public interface ITurnoService {
    List<TurnoDto> findAllTurnos();
    List<TurnoDto> findTurnobyId(Integer id);

}
