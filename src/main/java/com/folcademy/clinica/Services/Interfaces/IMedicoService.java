package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDto;

import java.util.List;

public interface IMedicoService {

    List<MedicoDto> findAllMedicos();
    //List<PersonaDto> findAllMedicos();
    List<MedicoDto> findMedicosbyId(Integer id);
}
