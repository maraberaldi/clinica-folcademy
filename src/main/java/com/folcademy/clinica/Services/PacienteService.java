package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaService personaService;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper,PersonaService personaService) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaService=personaService;
    }

    public List<PacienteDto> findAllPacientes() {
        List<Paciente> pacientes = (List<Paciente>)  pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }
    public List<PacienteDto> findPacientebyId(Integer id) {
        List<PacienteDto> lista = new ArrayList<>();
        PacienteDto paciente = pacienteRepository.findById(id).map(pacienteMapper ::entityToDto).orElse(null);
        lista.add(paciente);
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe el paciente");
        return lista;
    }

    public PacienteDto Agregar(PacienteDto entity){
        entity.setId(null);
        /*if(entity.getDni()==null || entity.getDni().isEmpty())
            throw new ValidationException("El campo dni es obligatorio");*/
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }
    public PacienteEnteroDto Editar(Integer idPaciente,PacienteEnteroDto dto){
        /*if(dto.getDni() != null || !dto.getDni().isEmpty())
            throw new ValidationException("El campo dni no se puede editar");*/
        if(!pacienteRepository.existsById(idPaciente))
           throw new NotFoundException("No existe el paciente");
        dto.setIdpaciente(idPaciente);
        return
                pacienteMapper.entityToEnteroDto(
                        pacienteRepository.save(
                                pacienteMapper.enterodtoToEntity(
                                        dto
                                )
                        )
                );
    }
    public boolean Eliminar(Integer id){
        if(!pacienteRepository.existsById(id))
          throw new NotFoundException("No existe el paciente");
        pacienteRepository.deleteById(id);
        return true;
    }
    public Page<PacienteEnteroDto> findAllByPage(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToEnteroDto);
    }
    public Page<PacienteDto> findByIdPage(Integer id,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize);
        return pacienteRepository.findById(id,pageable).map(pacienteMapper::entityToDto);
    }
}
