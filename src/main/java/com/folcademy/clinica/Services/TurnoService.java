package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")

public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository,TurnoMapper turnoMapper ) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }
    public List<TurnoDto> findAllTurnos() {
        List<Turno> turno = (List<Turno>)  turnoRepository.findAll();
        return turno.stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }
    public List<TurnoDto> findTurnobyId(Integer id) {
        List<TurnoDto> lista = new ArrayList<>();
        TurnoDto turno = turnoRepository.findById(id).map(turnoMapper ::entityToDto).orElse(null);
        lista.add(turno);
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe el turno");
        return lista;
    }
    public Page<TurnoDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }
    public Page<TurnoDto> findByIdPage(Integer id,Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findById(id,pageable).map(turnoMapper::entityToDto);
    }
    public TurnoDto Agregar(TurnoDto entity){
        entity.setIdturno(null);
        if(entity.getFecha() == null || entity.getHora() == null)
            throw new ValidationException("Los campos fecha y hora son obligatorios");
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }
    public TurnoDto Editar(Integer idTurno, TurnoDto dto){
        if(!turnoRepository.existsById(idTurno))
            throw new NotFoundException("No existe el turno");
        dto.setIdturno(idTurno);
        return turnoMapper.entityToDto(
                        turnoRepository.save(
                                turnoMapper.dtoToEntity(
                                        dto
                                )
                        )
                );
    }
    public String Eliminar(Integer id){
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe el turno");
        turnoRepository.deleteById(id);
        return "Se elimino el turno";
    }
}
