package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaService personaService;
    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper,PersonaService personaService) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaService=personaService;
    }

    public List<MedicoDto> findAllMedicos() {
        List<Medico> medicos = (List<Medico>)  medicoRepository.findAll();
        return medicos.stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    public List<MedicoDto> findMedicosbyId(Integer id) {
        List<MedicoDto> lista = new ArrayList<>();
        MedicoDto medico = medicoRepository.findById(id).map(medicoMapper ::entityToDto).orElse(null);
        lista.add(medico);
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe el medico");
        return lista;
    }
    public MedicoEnteroDto Agregar(MedicoEnteroDto entity){
        entity.setId(null);
        if(entity.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enterodtoToEntity(entity)));
    }
    public MedicoEnteroDto Editar(Integer idMedico,MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw new NotFoundException("No existe el medico");
        dto.setId(idMedico);
        return
                medicoMapper.entityToEnteroDto(
                        medicoRepository.save(
                                medicoMapper.enterodtoToEntity(
                                        dto
                                )
                        )
                );
    }
    public Boolean Eliminar(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe el medico a eliminar");
        medicoRepository.deleteById(id);
        return true;
    }

    /*public Page<MedicoDto> findAllByPage(Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize);
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }*/

    public Page<MedicoEnteroDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToEnteroDto);
    }
    public Page<MedicoDto> findByIdPage(Integer id,Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return medicoRepository.findById(id,pageable).map(medicoMapper::entityToDto);
    }
}
