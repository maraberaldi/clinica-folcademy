package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPersonaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("personaService")
public class PersonaService implements IPersonaService {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PersonaService(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }
    public List<PersonaDto> findAllPersonas() {
        List<Persona> persona = (List<Persona>)  personaRepository.findAll();
        return persona.stream().map(personaMapper::entityToDto).collect(Collectors.toList());
    }
    public List<PersonaDto> findPersonabyId(Integer id) {
        List<PersonaDto> lista = new ArrayList<>();
        PersonaDto persona = personaRepository.findById(id).map(personaMapper ::entityToDto).orElse(null);
        lista.add(persona);
        if(!personaRepository.existsById(id))
            throw new NotFoundException("No existe la persona");
        return lista;
    }
    public Page<PersonaDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return personaRepository.findAll(pageable).map(personaMapper::entityToDto);
    }
    public Page<PersonaDto> findByIdPage(Integer id,Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable= PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return personaRepository.findById(id,pageable).map(personaMapper::entityToDto);
    }

}
