package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity.ok(pacienteService.findAllPacientes());
    }

    @GetMapping(value = "/page")
    public ResponseEntity<Page<PacienteEnteroDto>> findAllByPage(
            @RequestParam(name="pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1") Integer pageSize
            //@RequestParam(name="orderField",defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity.ok(pacienteService.findAllByPage(pageNumber,pageSize));
    }
    @GetMapping(value ="/{ids}")
    public ResponseEntity<Page<PacienteDto>> findByIdPage(
            @PathVariable(name = "ids") int ids,
            @RequestParam(name="pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1") Integer pageSize
            //@RequestParam(name="orderField",defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity.ok(pacienteService.findByIdPage(ids,pageNumber,pageSize));

    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity){
        return ResponseEntity.ok(pacienteService.Agregar(entity));
    }
    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idPaciente}")

    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente") int id,
                                                    @RequestBody @Validated PacienteEnteroDto dto){
        return ResponseEntity.ok(pacienteService.Editar(id,dto));
    }
    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id){
        return ResponseEntity.ok(pacienteService.Eliminar(id));
    }
}
