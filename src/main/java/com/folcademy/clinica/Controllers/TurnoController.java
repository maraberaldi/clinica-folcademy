package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<TurnoDto>> findAll() {
        return ResponseEntity.ok(turnoService.findAllTurnos());
    }

    @GetMapping(value = "/page")
    public ResponseEntity<Page<TurnoDto>> findAllByPage(
            @RequestParam(name="pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "fecha") String orderField
    ) {
        return ResponseEntity.ok(turnoService.findAllByPage(pageNumber,pageSize,orderField));
    }
    @GetMapping(value ="/{ids}")
    public ResponseEntity<Page<TurnoDto>> findByIdPage(
            @PathVariable(name = "ids") int ids,
            @RequestParam(name="pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "fecha") String orderField
    ) {
        return ResponseEntity.ok(turnoService.findByIdPage(ids,pageNumber,pageSize,orderField));

    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity){
        return ResponseEntity.ok(turnoService.Agregar(entity));
    }
    @PutMapping("/{idTurno}")

    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idTurno") int id,
                                                 @RequestBody @Validated TurnoDto dto){
        return ResponseEntity.ok(turnoService.Editar(id,dto));
    }
    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<String> eliminar(@PathVariable(name = "idTurno") int id){
        return ResponseEntity.ok(turnoService.Eliminar(id));
    }
}
