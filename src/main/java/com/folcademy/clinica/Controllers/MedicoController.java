package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {this.medicoService = medicoService;}

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<MedicoDto>> findAll() {
        return ResponseEntity.ok(medicoService.findAllMedicos());
    }

    /*@GetMapping(value = "/page")
    public ResponseEntity<Page<MedicoDto>> findAllByPage(
            @RequestParam(name="pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "2") Integer pageSize
            //@RequestParam(name="orderField",defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity.ok(medicoService.findAllByPage(pageNumber,pageSize));

    }*/

    @GetMapping("/page")
    public ResponseEntity<Page<MedicoEnteroDto>> findAllByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "profesion") String orderField
    ){
        return ResponseEntity.ok(medicoService.findAllByPage(pageNumber,pageSize,orderField));
    }

    /*@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<MedicoDto>> findById(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok(medicoService.findMedicosbyId(id));
    }*/

    @GetMapping(value ="/{ids}")
    public ResponseEntity<Page<MedicoDto>> findByIdPage(
            @PathVariable(name = "ids") int ids,
            @RequestParam(name="pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name="orderField",defaultValue = "profesion") String orderField
    ) {
        return ResponseEntity.ok(medicoService.findByIdPage(ids,pageNumber,pageSize,orderField));

    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody @Validated MedicoEnteroDto entity){
        return ResponseEntity.ok(medicoService.Agregar(entity));
    }
    @PutMapping("/{idMedico}")

    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody @Validated MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.Editar(id,dto));
    }
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id){
        return ResponseEntity.ok(medicoService.Eliminar(id));
    }
}